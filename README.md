# pplacer docker image #

pplacer places query sequences on a fixed reference phylogenetic tree to maximize phylogenetic likelihood or posterior probability according to a reference alignment.

pplacer vertion of v1.1.alpha19 is installed in this image.

all pplacer executables are in program search path, and pplacer is set to be the default program.

To run pplacer:
docker run --rm --volume:/MY_PATH:/input pplacer:latest ARGS

To run other programes:
docker run --rm --volume:/MY_PATH:/input pplacer:latest guppy ARGS