FROM debian:jessie
LABEL Maintainer Shijie Yao, syao@lbl.gov

# make alias active
RUN sed -i 's/# alias/alias/' ~/.bashrc

# install the needed programs
ENV PACKAGES wget unzip
RUN apt-get update && apt-get install --yes ${PACKAGES}

# create download and move into it
WORKDIR /workdir

# download and install pplacer 
RUN wget https://github.com/matsen/pplacer/releases/download/v1.1.alpha19/pplacer-linux-v1.1.alpha19.zip 
RUN unzip pplacer-linux-v1.1.alpha19.zip 	&& \
	rm -rf pplacer-linux-v1.1.alpha19.zip 	&& \
	mv pplacer-Linux-v1.1.alpha19/* /usr/bin/.	&& \
	rm -rf pplacer-Linux-v1.1.alpha19

CMD pplacer
